#include <avr/io.h>

#define CPU_PRESCALE(n) (CLKPR = 0x80, CLKPR = (n))
#define CPU_16Mhz 0x00
#define F_CPU 16000000L

int main(void){

	CPU_PRESCALE(CPU_16Mhz);

	//Set data direction
	DDRD |= (1 << PD7);
	DDRD &= ~(1 << PD3);

	while(1){
		//Read the pin
		int val = PIND;
		if (val & (1 << PD3)){
			//Turn off the LED
			PORTD &= ~(1 << PD7);
		}else{
			//Turn on the LED
			PORTD |= (1 << PD7);
		}
	
	}


}