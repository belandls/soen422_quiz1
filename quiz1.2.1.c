#include <avr/io.h>
#include <avr/interrupt.h>
#include "usb_serial.h"
#define CPU_PRESCALE(n) (CLKPR = 0x80, CLKPR = (n))
#define CPU_16Mhz 0x00
#define F_CPU 16000000L

//Sends an int value to the Console through serial comm.
void sendToSerial(int val){
	char buffer[100];
	int cx;
	cx = snprintf(buffer, 99, "the val is %d\n", val);
	buffer[cx] = '\0';
	usb_serial_write(buffer,cx);
}

//Sets up Timer1 for blinking
void SetupTimerForBlinking(){
	//Reset Timer1's registers
	TCCR1A = 0; 
	TCCR1B = 0;
	//Sets Timer1 in phase and frequency correct with ICR for top and a 1024 prescaler. OC1A is set to be inverted (set on rising, clear on downcount)
	TCCR1A |= (1 << COM1A1) | (1 << COM1A0) ;
	TCCR1B |= (1 << WGM13) | (1 << CS12) | (1 << CS10);
	//Set the TOP just a bit more than OCR1A (initial 15625) so that a small blink is created
	ICR1 = 16000;
}

//Sets up Timer1 to control itensity
void SetupTimerForIntensity(){
	//Reset Timer1's registers
	TCCR1A = 0;
	TCCR1B = 0;
	//Sets Timer1 in phase and frequency correct with ICR for top and a 1 prescaler.
	TCCR1A |=  (1 << COM1B1);
	TCCR1B |= (1 << WGM13) | (1 << CS10);
	//Sets the max to 255 to simulate the 8bit timer(faster)
	ICR1 = 255;
}

//Sets up Timer3 to control the reading
void SetupTimer3ForReading(){
	//Enable Overflow interrupt and Compare Match interrupt
	TIMSK3 |= (1 << TOIE3) | (1 << OCIE3A);
	
	//Sets Timer3 in Fast PWM with a 1024 prescaler and TOP is ICR. No need for OC3X pins
	TCCR3A |=  (1 << WGM31);
	TCCR3B |= (1 << WGM33) | (1 << WGM32) | (1 << CS32) | (1 << CS30);
	//Set ADC to use internal clock and to use channel 1 (F1)
	ADMUX |= (1 << REFS1) | (1 << REFS0) | (1 << MUX0);
	//Enables the ADC and sets the prescaler at 128.
	ADCSRA |= (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
	
	ICR3 = 15625; //For 1 seconds interval
	OCR3A = 15575; //To interrupt just a bit before the overflow int.
}

//Sets new blinking rate based on that 15625 timer cycles = 2 seconds
void SetNewValueForBlinking(long val){
	if(val >= 100 && val <= 2000){
		long newVal = val * 15625 / 2000;
		OCR1A = newVal;
		ICR1 = newVal + 300; //for a little blink
	}
}

//Sets new duty cycle by converting from 0-255 to 0-100%
void SetNewValueForIntensity(long val){
	if(val >= 0 && val <= 100){
		long newVal = val * 255 / 100;
		OCR1B = newVal;
	}
}

//Sets new reading rate based on that 31250 timer cycles = 2 seconds (fastPWM)
void SetNewValueForReading(long val){
	if(val >= 100 && val <= 2000){
		long newVal = val * 31250 / 2000;
		ICR3 = newVal;
		OCR3A = newVal = 50;
	}
}

//Send conversion result to console
ISR(TIMER3_OVF_vect){
	if (ADCSRA & (1 << ADIF)){
		int val = ADC;
		ADCSRA &= (1 << ADIF); //Clear the int. flag
		sendToSerial(val);
	} else {
		sendToSerial(-1);
	}
}

//start the conversion
ISR(TIMER3_COMPA_vect){
	ADCSRA |= (1 << ADSC);
}

int main(void){

	CPU_PRESCALE(CPU_16Mhz);

	//Set data direction

	usb_init();
	
	SetupTimer3ForReading();//Setup Timer3 for reading
	SetupTimerForBlinking();//Initial mode
	//Set data direction for pins B5 and B6 to OUTPUT
	DDRB |= (1 << PB5) | (1 << PB6);
	
	OCR1A = 15625;	//Initial blink rate (2 seconds)
		
	int i = 0; //read counter
	long val = 0; //converted value buffer
	char read[100]; //Read buffer
	char mode = 1; //Current mode (1=blinking, 2=Intensity)
	while(1){
		while(usb_serial_available()){
			read[i] = usb_serial_getchar();
			i++;
		}
		read[i] = '\0';
		i = 0;
		if (read[0] == 'F'){
			if (mode != 1){ //Switch mode if different
				SetupTimerForBlinking();
				mode = 1;
			}
			val = atoi(read + 1);
			SetNewValueForBlinking(val);
			
		} else if (read[0] == 'I'){
			if (mode != 2){ //Switch mode if different
				SetupTimerForIntensity();
				mode = 2;
			}
			val = atoi(read + 1);
			SetNewValueForIntensity(val);
			
		}  else if (read[0] == 'R'){
			val = atoi(read + 1);
			SetNewValueForReading(val);
		}
	
	}


}