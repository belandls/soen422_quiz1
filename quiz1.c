#include <avr/io.h>
#include "usb_serial.h"
#define CPU_PRESCALE(n) (CLKPR = 0x80, CLKPR = (n))
#define CPU_16Mhz 0x00
#define F_CPU 16000000L

//Sets up Timer1 for blinking
void SetupTimerForBlinking(){
	//Reset Timer1's registers
	TCCR1A = 0; 
	TCCR1B = 0;
	//Sets Timer1 in phase and frequency correct with ICR for top and a 1024 prescaler. OC1A is set to be inverted (set on rising, clear on downcount)
	TCCR1A |= (1 << COM1A1) | (1 << COM1A0) ;
	TCCR1B |= (1 << WGM13) | (1 << CS12) | (1 << CS10);
	//Set the TOP just a bit more than OCR1A (initial 15625) so that a small blink is created
	ICR1 = 16000;
}

//Sets up Timer1 to control itensity
void SetupTimerForIntensity(){
	//Reset Timer1's registers
	TCCR1A = 0;
	TCCR1B = 0;
	//Sets Timer1 in phase and frequency correct with ICR for top and a 1 prescaler.
	TCCR1A |=  (1 << COM1B1);
	TCCR1B |= (1 << WGM13) | (1 << CS10);
	//Sets the max to 255 to simulate the 8bit timer(faster)
	ICR1 = 255;
}

//Sets new blinking rate based on that 15625 timer cycles = 2 seconds
void SetNewValueForBlinking(long val){
	if(val >= 100 && val <= 2000){
		long newVal = val * 15625 / 2000;
		OCR1A = newVal;
		ICR1 = newVal + 300; //for a little blink
	}
}

//Sets new duty cycle by converting from 0-255 to 0-100%
void SetNewValueForIntensity(long val){
	if(val >= 0 && val <= 100){
		long newVal = val * 255 / 100;
		OCR1B = newVal;
	}
}

int main(void){
	CPU_PRESCALE(CPU_16Mhz);
	usb_init();
	SetupTimerForBlinking();//Initial mode
	//Set data direction for pins B5 and B6 to OUTPUT
	DDRB |= (1 << PB5) | (1 << PB6);
	
	OCR1A = 15625;	//Initial blink rate (2 seconds)
		
	int i = 0; //read counter
	long val = 0; //converted value buffer
	char read[100]; //Read buffer
	char mode = 1; //Current mode (1=blinking, 2=Intensity)
	while(1){
		while(usb_serial_available()){
			read[i] = usb_serial_getchar();
			i++;
		}
		read[i] = '\0';
		i = 0;
		if (read[0] == 'F'){
			if (mode != 1){ //Switch mode if different
				SetupTimerForBlinking();
				mode = 1;
			}
			val = atoi(read + 1);
			SetNewValueForBlinking(val);
			
		} else if (read[0] == 'I'){
			if (mode != 2){  //Switch mode if different
				SetupTimerForIntensity();
				mode = 2;
			}
			val = atoi(read + 1);
			SetNewValueForIntensity(val);
			
		}
	
	}


}