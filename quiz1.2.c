#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "usb_serial.h"
#define CPU_PRESCALE(n) (CLKPR = 0x80, CLKPR = (n))
#define CPU_16Mhz 0x00
#define F_CPU 16000000L

void sendToSerial(int val){
	char buffer[100];
	int cx;
		
	cx = snprintf(buffer, 99, "the val is %d\n", val);
	buffer[cx] = '\0';
	usb_serial_write(buffer,cx);
}

//Send conversion result to console
ISR(TIMER1_OVF_vect){
	if (ADCSRA & (1 << ADIF)){
		int val = ADC;
		ADCSRA &= ~(1 << ADIF);
		sendToSerial(val);
	} else {
		sendToSerial(-1);
	}
}

//start the conversion
ISR(TIMER1_COMPA_vect){
	ADCSRA |= (1 << ADSC);
}

int main(void){

	CPU_PRESCALE(CPU_16Mhz);

	//Set data direction

	usb_init();
	
	TIMSK1 |= (1 << TOIE1) | (1 << OCIE1A);
	
	TCCR1A |= (1 << COM1A0) | (1 << WGM11);
	TCCR1B |= (1 << WGM13) | (1 << WGM12) | (1 << CS12) | (1 << CS10);
	
	ADMUX |= (1 << REFS1) | (1 << REFS0) | (1 << MUX0);
	ADCSRA |= (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
	
	ICR1 = 15625;
	OCR1A = 15575;

	
		
	int i = 0;
	long val = 0;
	char read[100];
	while(1){
		while(usb_serial_available()){
			read[i] = usb_serial_getchar();
			i++;
		}
		
		read[i] = '\0';
		val = atoi(read);
		i = 0;
		if(val >= 100 && val <= 2000){
			long newVal = val * 31250 / 2000;
			ICR1 = newVal;
			OCR1A = newVal = 50;
		}
	
	}


}